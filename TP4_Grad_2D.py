import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal
from scipy import misc

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee2.sav')

image = sav["photo"]
data = sav["data"]
h = sav["ker"]

print(data.shape)

N = image.shape[0]
fft_h = np.fft.fft2(h)

def Gradient_desc(y, h, N, alpha):
    
    # Step 1
    x = np.random.random((N,N))
    h_transp = np.fliplr(np.flipud(h))
    it = 0

    # Trouver un meilleur critère d'arrêt ? 
    while it < 200:
        # Step 2 
        # mode='same' car on veut garder les même dimension que y (256x256) pour l'étape suivante
        Hx = signal.convolve2d(x, h, mode='same')

        # Step 3
        # dim(y) = 256*256, on veut donc que Hx soit de même dim
        err = (Hx - y)

        # Step 4
        HxErr = signal.convolve2d(err, h_transp, mode='same')

        # Step 5 
        x = x - alpha*HxErr

        it = it+1

    return x

def plot_gradient_desc(image, data, h, N):
    x_1 = Gradient_desc(data, h, N, 1.5)
    x_2 = Gradient_desc(data, h, N, 0.015)
    x_3 = Gradient_desc(data, h, N, 2.03)

    fig, ax = plt.subplots(1,3)
    #ax[0].imshow(np.flipud(image))
    #ax[1].imshow(np.flipud(data))
    ax[0].imshow(np.flipud(x_1))
    ax[1].imshow(np.flipud(x_2))
    ax[2].imshow(np.flipud(x_3))

    #ax[0].title.set_text('Image réelle')
    #ax[1].title.set_text('Data')
    ax[0].title.set_text('alpha = 1.5')
    ax[1].title.set_text('alpha = 0.015')
    ax[2].title.set_text('alpha = 2.03')

    plt.show()


plot_gradient_desc(image, data, h, N)

