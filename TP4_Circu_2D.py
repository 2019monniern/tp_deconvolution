import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee1.sav')

image = sav["photo"]
data = sav["data"]
h = sav["fftker"] # h est déjà bien construit 


# FFT(h) = lamda
lamda = np.fft.fft2(h)

# Calcul de Gmc_ij = 1/lamda_ij
Gmc = np.zeros((250,250), dtype=lamda.dtype)
for i in range(250):
    for j in range(250):
        Gmc[i][j] = 1.0/lamda[i][j]

# FFT2D de y 
Y = np.fft.fft2(data)

# Calcul de X_app
X_app = np.multiply(Gmc,Y)

# Calcul de x_app 
x_app = np.abs(np.fft.ifft2(X_app))

fig, ax = plt.subplots(1,3)
ax[0].imshow(np.flipud(image))
ax[1].imshow(np.flipud(data))
ax[2].imshow(np.flipud(x_app))

ax[0].title.set_text('Image réelle')
ax[1].title.set_text('data')
ax[2].title.set_text('Image reconstruite')

plt.show()

