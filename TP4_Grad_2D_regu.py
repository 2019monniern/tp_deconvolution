import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal
from scipy import misc

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee2.sav')

image = sav["photo"]
data = sav["data"]
h = sav["ker"]

N = image.shape[0]
fft_h = np.fft.fft2(h)


def alpha_opt(grad_x, h, Mu, D1, D2):
    num_alpha = np.sum(np.sum(grad_x**2))

    denom_alpha = np.sum(signal.convolve2d(grad_x, h, mode='same')**2 +
                     Mu*(signal.convolve2d(grad_x, D1, mode='same')**2 +
                         signal.convolve2d(grad_x, D2, mode='same')**2))

    return num_alpha/denom_alpha


def Gradient_desc(y, h, N, alpha, Mu):
    
    # Step 1
    x = np.random.random((N,N))
    h_transp = np.fliplr(np.flipud(h))
    it = 0

    D1 = np.array([[0, 0, 0],
                   [0, 1, -1],
                   [0, 0, 0]])

    D2 = np.array([[0, 0, 0],
                   [0, 1, 0],
                   [0, -1, 0]])

    norm_gradient = 20
    print(2/(np.linalg.norm(h**2)))
    # Ici nombre d'itération pris de manière alétatoire
    while it < 300 and norm_gradient > 9.0 :
        # Step 2 
        # mode='same' car on veut garder les même dimension que y (256x256) pour l'étape suivante
        Hx = signal.convolve2d(x, h, mode='same')

        # Step 3
        err = (Hx - y)

        # Step 4 - Ht(Hx_k - y)
        HxErr = signal.convolve2d(err, h_transp, mode='same')


        D1x = signal.convolve2d(x, D1, mode='valid')
        D2x = signal.convolve(x, D2, mode='valid')

        D1tD1x = signal.convolve2d(D1x, np.transpose(D1), mode='full')
        D2tD2x = signal.convolve2d(D2x, np.transpose(D2), mode='full')

        # Calcul alpha_opt
        grad_x = HxErr + Mu*(D1tD1x + D2tD2x)
        alpha = alpha_opt(grad_x, h, Mu, D1, D2)

        # Step 5 - X_k+1 = x_k - alpha*[Ht(Hx_k - y) + MU*( D1tD1x_k + D2tD2x_k)]
        x = x - alpha*(grad_x)
       
        norm_gradient = np.linalg.norm(grad_x)
        print(norm_gradient)
        it = it+1

    print("Total number of iteration = %d"%it)
    return x



def plot_gradient_desc(image, data, h, N):
    x_1 = Gradient_desc(data, h, N, 1.5, 0)
    x_2 = Gradient_desc(data, h, N, 1.5, 0.0005)
    x_3 = Gradient_desc(data, h, N, 1.5, 0.0015)

    fig, ax = plt.subplots(1,3)
    #ax[0].imshow(np.flipud(image))
    #ax[1].imshow(np.flipud(data))
    ax[0].imshow(np.flipud(x_1))
    ax[1].imshow(np.flipud(x_2))
    ax[2].imshow(np.flipud(x_3))


    #ax[0].title.set_text('Image réelle')
    #ax[1].title.set_text('Data')
    ax[0].title.set_text('Mu = 0')
    ax[1].title.set_text('Mu = 0.002')
    ax[2].title.set_text('Mu = 0.005')

    plt.show()


plot_gradient_desc(image, data, h, N)

