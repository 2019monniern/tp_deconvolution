import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee1.sav')

image = sav["photo"]
data = sav["data"]
h = sav["fftker"] # h est déjà bien construit 


def circulante(data, h, Mu):
    lamda = np.fft.fft2(h)

    # D1 & D1_fft
    D1 = np.array([0, 1, -1])
    D1 = np.block([[D1, np.zeros((1, 247))],[np.zeros((249,250))]])
    D1_FFT = np.fft.fft2(D1)

    # D2 & D2_fft
    D2 = np.transpose(D1)
    D2_FFT = np.fft.fft2(D2)

    # Calcul de Gmc_ij = (lamda_ij)*/(abs(lamda_ij)**2 + Nu*abs(Dfft_ij)**2)
    Gmc = np.zeros((250,250), dtype=lamda.dtype)
    for i in range(250):
        for j in range(250):
            Gmc[i][j] = (np.conj(lamda[i][j]))/(abs(lamda[i][j])**2 + Mu*abs(D2_FFT[i][j]+D1_FFT[i][j])**2)  # Comment construire Gmc avec D1 et D2 au lieu de D ? 

    # FFT2D de y 
    Y = np.fft.fft2(data)

    # Calcul de X_app - Multiplication élément par élément
    X_app = np.multiply(Gmc,Y)

    # Calcul de x_app 
    x_app = np.abs(np.fft.ifft2(X_app))

    return x_app

def distance_L1(x_app, x):
    return np.linalg.norm((x_app - x), 1)

def distance_L2(x_app, x):
    return np.linalg.norm((x_app - x), 2)

def distance_Linf(x_app, x):
    return np.linalg.norm((x_app - x), np.inf)


def get_Mu_opt(image, data, h):
    #Resize real image to be 250 x 250
    resized_image = np.zeros((250,250))
    for i in range(250):
        for j in range(250):
            resized_image[i][j] = image[i+3][j+3]

    Mu = np.arange(start=0.01, stop=0.2, step=0.001)
    nb_sample = len(Mu)

    #L1_dist = {Mu[i] : -1 for i in range(nb_sample)}
    L2_dist = {Mu[i] : -1 for i in range(nb_sample)}
    Linf_dist = {Mu[i] : -1 for i in range(nb_sample)}
    for i in range(nb_sample):
        x_app = circulante(data, h, Mu[i])
        L1_dist[Mu[i]] = distance_L1(x_app, resized_image)
        L2_dist[Mu[i]] = distance_L2(x_app, resized_image)
        Linf_dist[Mu[i]] = distance_Linf(x_app, resized_image)

    print("---------------------------------")
    print(min(L1_dist, key=L1_dist.get))
    print(min(L2_dist, key=L2_dist.get))
    print(min(Linf_dist, key=Linf_dist.get))

    """"
    Les valeurs obtenues de Mu qui minimisent les différents distances sont :
    Mu = 0.001744 Pour L1 (correspond plutôt bien aux observations)
    Mu = 0.062 Pour L2 (Image plutôt floutée)
    Mu = 0.194 Pour Linf (Image complètement floutée)
    """


def Bias_square(x, h, y, Mu):

    x_fft = np.fft.fft2(x)
    lamda = np.fft.fft2(h)

    # D1 & D1_fft
    D1 = np.array([0, 1, -1])
    D1 = np.block([[D1, np.zeros((1, 247))],[np.zeros((249,250))]])
    D1_FFT = np.fft.fft2(D1)

    # D2 & D2_fft
    D2 = np.transpose(D1)
    D2_FFT = np.fft.fft2(D2)

    res = np.zeros(h.shape)

    for i in range ():
        for j in range():
            res[i][j] = ((Mu * abs(D1_FFT[i][j] + D2_FFT[i][j])**2)/( abs(lamda[i][j])**2 + Mu*abs(D1_FFT[i][j] + D2_FFT[i][j])**2 ))**2 * x_FFT[i][j]**2

    return res








fig, ax = plt.subplots(1,3)
#ax[0].imshow(np.flipud(image))
#ax[1].imshow(np.flipud(data))
#ax[2].imshow(np.flipud(x_app))

ax[0].title.set_text('Image réelle')
ax[1].title.set_text('data')
ax[2].title.set_text('Image reconstruite')

#plt.show()

