import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee1.sav')

image = sav["photo"]
data = sav["data"]
h = sav["fftker"] # h est déjà bien construit 

# FFT(h) = lamda
lamda = np.fft.fft2(h)


# D1 & D1_fft
D1 = np.array([0, 1, -1])
D1 = np.block([[D1, np.zeros((1, 247))],[np.zeros((249,250))]])
D1_FFT = np.fft.fft2(D1)
# D2 & D2_fft
D2 = np.transpose(D1)
D2_FFT = np.fft.fft2(D2)


# Calcul de Gmc_ij = (lamda_ij)*/(abs(lamda_ij)**2 + Nu*abs(Dfft_ij)**2)
Gmc = np.zeros((250,250), dtype=lamda.dtype)
for i in range(250):
    for j in range(250):
        Gmc[i][j] = (np.conj(lamda[i][j]))/(abs(lamda[i][j])**2 + 0.00174*abs(D2_FFT[i][j]+D1_FFT[i][j])**2)   


# FFT2D de y 
Y = np.fft.fft2(data)

# Calcul de X_app - Multiplication élément par élément
X_app = np.multiply(Gmc,Y)

# Calcul de x_app 
x_app = np.abs(np.fft.ifft2(X_app))


fig, ax = plt.subplots(1,3)
ax[0].imshow(np.flipud(image))
ax[1].imshow(np.flipud(data))
ax[2].imshow(np.flipud(x_app))

ax[0].title.set_text('Image réelle')
ax[1].title.set_text('data')
ax[2].title.set_text('Image reconstruite')

plt.show()

