import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal
from scipy import misc

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee2.sav')

image = sav["photo"]
data = sav["data"]
h = sav["ker"]


N = image.shape[0]
fft_h = np.fft.fft2(h)


def hesHuber(x, seuil):
    return np.where(np.abs(x) < seuil, 2, 0)

"""
def alpha_opt(grad_x, h, x, Mu, D1, D2, seuil):
    num_alpha = np.sum(grad_x**2)

    phi_D1 = hesHuber(signal.convolve2d(x, D1, mode='same'), seuil)
    phi_D2 = hesHuber(signal.convolve2d(x, D2, mode='same'), seuil)

    denom_alpha = (np.sum(signal.convolve2d(x, h, mode='same')**2) +
                    Mu*(np.sum(signal.convolve2d(grad_x, D1, mode='same')**2 * phi_D1) +
                        np.sum(signal.convolve2d(grad_x, D2, mode='same')**2 * phi_D2)))
    
    return num_alpha/denom_alpha
"""
def alpha_opt(grad_x, h, x, Mu, D1, D2, seuil):

    hgk = signal.convolve2d(grad_x, h, mode='valid')

    d1x = signal.convolve2d(x, D1, mode='valid')
    d1g = signal.convolve2d(grad_x, D1, mode='valid')
    phi1 = hesHuber(d1x, seuil)

    dd1x = np.sum((d1g**2)*phi1)

    d2x = signal.convolve2d(x, D2, mode='valid')
    d2g = signal.convolve2d(grad_x, D2, mode='valid')
    #phi2 = d2g*hesHuber(d2x, seuil) # pkoi *d2g
    phi2 = hesHuber(d2x, seuil)
    dd2x = np.sum((d2g**2)*phi2)

    denom_alpha = 2*np.sum(hgk**2) + Mu*(dd1x+dd2x)
    num_alpha = np.sum(grad_x**2)

    alpha = num_alpha/denom_alpha
    return alpha


def gradHuber(x, seuil):
    i_max, j_max = x.shape
    grad = np.zeros(x.shape)
    inv_seuil = -seuil

    for i in range(i_max):
        for j in range(j_max):
            if x[i][j] < inv_seuil:
                grad[i][j] = 2*inv_seuil
            elif abs(x[i][j]) <= seuil:
                grad[i][j] = 2*x[i][j]
            elif x[i][j] > seuil:
                grad[i][j] = 2*seuil
    
    return grad


def Grad_Desc_Huber(y, h, N, alpha, Mu, seuil):
    # Step 1 - set random x0
    x = np.random.random((N,N))
    h_transp = np.fliplr(np.flipud(h))
    it = 0

    D1 = np.array([[0, 0, 0],
                   [0, 1, -1],
                   [0, 0, 0]])

    D2 = np.array([[0, 0, 0],
                   [0, 1, 0],
                   [0, -1, 0]])

    #seuil = 10
    norm_gradient = 1000
    while it < 100 and norm_gradient > 9.0: 
        # Step 2 
        Hx = signal.convolve2d(x, h, mode='same')

        # Step 3 
        err = Hx - y

        # Step 4
        HtErr = signal.convolve2d(err, h_transp, mode='same')
    
        # Step 5
        D1x = signal.convolve2d(x, D1, mode='valid')
        D2x = signal.convolve2d(x, D2, mode='valid')
        
        # Step 6
        d_Hub_D1 = gradHuber(D1x, seuil)
        d_Hub_D2 = gradHuber(D2x, seuil)

        # Step 7
        D1tdHub = signal.convolve2d(d_Hub_D1, np.transpose(D1), mode='full')
        D2tdHub = signal.convolve2d(d_Hub_D2, np.transpose(D2), mode='full')


        grad_x = HtErr + Mu*(D1tdHub + D2tdHub)

        # Step 8 -  Update alpha
        alpha = alpha_opt(grad_x, h, x, Mu, D1, D2, seuil)
        #alpha = 1.5
        # Step 9 
        x = x - alpha*(grad_x)

        norm_gradient = np.linalg.norm(grad_x)
        print(norm_gradient)
        it = it+1

    print("Total number of iteration = %d"%it)
    return x

        

def plot_gradient_desc(image, data, h, N):
    x_1 = Grad_Desc_Huber(data, h, N, 1.5, 0.0017, 2)
    x_2 = Grad_Desc_Huber(data, h, N, 1.5, 0.0017, 10)
    x_3 = Grad_Desc_Huber(data, h, N, 1.5, 0.0017, 20)

    fig, ax = plt.subplots(1,3)
    #ax[0].imshow(np.flipud(image))
    #ax[1].imshow(np.flipud(data))
    ax[0].imshow(np.flipud(x_1))
    ax[1].imshow(np.flipud(x_2))
    ax[2].imshow(np.flipud(x_3))


    #ax[0].title.set_text('Image réelle')
    #ax[1].title.set_text('Data')
    ax[0].title.set_text('Mu = 0')
    ax[1].title.set_text('Mu = 0.002')
    ax[2].title.set_text('Mu = 0.005')

    plt.show()


plot_gradient_desc(image, data, h, N)

