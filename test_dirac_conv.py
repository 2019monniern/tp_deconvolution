import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy import signal

# Import data from 'donnee1'
sav = scipy.io.readsav('donnee1.sav')

image = sav["photo"]
data = sav["data"]
h = sav["fftker"]


k_dirac = np.array([[0,0,0],[0,0,0],[0,0,1]])

K = np.zeros(image.shape)
K[50][50] = 1

K_fft = np.fft.fft2(K)
image_fft = np.fft.fft2(image)

Y = K_fft*image_fft
y = abs(np.fft.ifft2(Y))

image_conv = signal.convolve2d(image, k_dirac, 'same')

fig, ax = plt.subplots(1,2)
ax[0].imshow(np.flipud(image))
ax[1].imshow(np.flipud(y))
plt.show()
